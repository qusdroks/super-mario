using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.GameManager;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay
{
    public class BoxGold : MonoBehaviour
    {
        [SerializeField] private LayerMask monsterLayerMask;
        [SerializeField] private GameObject fxDestroy;
        [SerializeField] private int score = 20;
        [SerializeField] private float radius = 1f;

        private Monster.Monster _monster;

        public void Break()
        {
            var t = transform;
            gameObject.SetActive(false);
            
            UIManager.Instance.AddScore(t, score);
            AudioManager.Instance.PlaySoundLoop("Sound Block");

            if (fxDestroy != null)
            {
                var ps = SpawnerHelper.CreateSpawner(t.position, null, fxDestroy).GetComponent<ParticleSystem>();
                ps.SetActive(true);
            }

            if (_monster != null)
            {
                _monster.Death();
            }
        }

        private void Update()
        {
            var hit = Physics2D.CircleCast(transform.position, radius, Vector2.up, radius, monsterLayerMask);

            if (hit.collider != null && hit.collider.CompareTag("Monster"))
            {
                _monster = hit.collider.GetComponent<Monster.Monster>();
            }
            else
            {
                _monster = null;
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawWireSphere(transform.position, radius);
        }
    }
}