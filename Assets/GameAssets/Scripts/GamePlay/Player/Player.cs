﻿using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.GameManager;
using GameAssets.Scripts.GameUI.UIPlay;
using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Player
{
    public partial class Player : BaseCharacter
    {
        [SerializeField] private GameObject bulletPrefab;
        [SerializeField] private Transform exitPoint;
        [SerializeField] private Color fillPhaseColor;

        [SerializeField] private float distance;
        [SerializeField] private float thrustForce;

        public static Player Instance;

        private readonly RaycastHit2D[] _hit2Ds = new RaycastHit2D[10];

        private UIPlay _uiPlay;
        private MaterialPropertyBlock _mtBlock;
        private Renderer _renderer;
        private Vector3 _checkPoint;
        private Vector3 _direction;

        private int _fillPhase;

        private bool _isJump;
        private bool _isDoubleJump;
        private bool _isFalling = true;
        private bool _isHurt;

        private void FlashColor(bool change)
        {
            _mtBlock.SetColor(_fillPhase, change ? Color.red : fillPhaseColor);
            _renderer.SetPropertyBlock(_mtBlock);
        }

        private void SingleShoot()
        {
            if (!GameManager.GameManager.Instance.IsGameState(GameState.Playing))
            {
                return;
            }

            UIManager.Instance.CanShoot(() =>
            {
                var bullet = SpawnerHelper.CreateSpawner(exitPoint.position, null, bulletPrefab);
                bullet.GetComponent<Bullet>().Shoot(transform.localScale.x >= 0f);
                AudioManager.Instance.PlaySoundLoop("Sound Shot");
            });
        }

        private void Jump()
        {
            if (!GameManager.GameManager.Instance.IsGameState(GameState.Playing))
            {
                return;
            }
			
			if (transform.parent.name.Equals("Platform"))
			{
				transform.SetParent(null);
			}

            void Action()
            {
                ChangeAnimation(2);
                rb.AddForce(Vector2.up * thrustForce, ForceMode2D.Force);
                AudioManager.Instance.PlaySoundLoop("Sound Jump");
            }

            switch (_isJump)
            {
                case true when !_isDoubleJump:
                    _isJump = false;
                    _isDoubleJump = true;
                    Action();
                    break;

                case false when _isDoubleJump:
                    _isDoubleJump = false;
                    Action();
                    break;
            }
        }

        private void StopJump()
        {
            _isFalling = true;
            ChangeAnimation(3);
        }

        private void MoveLeft()
        {
            if (!GameManager.GameManager.Instance.IsGameState(GameState.Playing))
            {
                return;
            }

            var scale = transform.localScale;

            if (scale.x > 0f)
            {
                scale.x *= -1;
            }

            stat.SetCharacterState(CharacterState.Move);
            ChangeAnimation(1);

            _direction = Vector3.left;
            transform.localScale = scale;
        }

        private void MoveRight()
        {
            if (!GameManager.GameManager.Instance.IsGameState(GameState.Playing))
            {
                return;
            }

            var scale = transform.localScale;

            if (scale.x < 0f)
            {
                scale.x *= -1;
            }

            stat.SetCharacterState(CharacterState.Move);
            ChangeAnimation(1);

            _direction = Vector3.right;
            transform.localScale = scale;
        }

        private void StopMove()
        {
            if (!GameManager.GameManager.Instance.IsGameState(GameState.Playing))
            {
                return;
            }

            rb.ResetInertia();
            stat.SetCharacterState(CharacterState.IdleOrRespawn);
            ChangeAnimation(_isFalling ||
                            Physics2D.Raycast(transform.position, Vector2.down * distance, distance, 1 << 8).collider ==
                            null
                ? 3
                : 0);
        }
    }
}