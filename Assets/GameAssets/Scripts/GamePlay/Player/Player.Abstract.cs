using System;
using Cysharp.Threading.Tasks;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.GameInterface;
using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Player
{
    public partial class Player : IStatus
    {
        public async void Hurt()
        {
            if (_isHurt)
            {
                return;
            }

            _isHurt = true;
            _uiPlay.LostHeart((int) stat.AddHp(-1));

            for (var i = 0; i < 40; i++)
            {
                FlashColor(i % 2 == 0);
                await UniTask.Delay(TimeSpan.FromSeconds(0.05f));
            }

            _isHurt = false;
        }

        public void HurtBullet()
        {
        }

        public void Death()
        {
            if (stat.Hp <= 0)
            {
                ChangeAnimation(4);
                stat.ChangeCharacterState(CharacterState.Death);
                GameManager.GameManager.Instance.OnLose();
            }
            else
            {
                transform.SetPosition(_checkPoint);
                ChangeAnimation(3);
                rb.ResetInertia();
                Hurt();
            }
        }
    }
}