using System;
using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.GameManager;
using GameAssets.Scripts.GameUI.UIPlay;
using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Player
{
    public partial class Player
    {
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }

            _uiPlay = UIManager.Instance.GetUI<UIPlay>();
            _mtBlock = new MaterialPropertyBlock();
            _renderer = GetComponent<Renderer>();
            _checkPoint = transform.position;
            _fillPhase = Shader.PropertyToID("_Black");
            ChangeAnimation(3);

            GameEvent.GameEvent.OnPlayerShoot += SingleShoot;
            GameEvent.GameEvent.OnPlayerJump += Jump;

            GameEvent.GameEvent.OnPlayerMoveLeft += MoveLeft;
            GameEvent.GameEvent.OnPlayerMoveRight += MoveRight;

            GameEvent.GameEvent.OnPlayerStopMove += StopMove;
            GameEvent.GameEvent.OnPlayerStopJump += StopJump;
        }

        private void OnDestroy()
        {
            if (Instance == this)
            {
                Instance = null;
            }

            GameEvent.GameEvent.OnPlayerShoot -= SingleShoot;
            GameEvent.GameEvent.OnPlayerJump -= Jump;

            GameEvent.GameEvent.OnPlayerMoveLeft -= MoveLeft;
            GameEvent.GameEvent.OnPlayerMoveRight -= MoveRight;

            GameEvent.GameEvent.OnPlayerStopMove -= StopMove;
            GameEvent.GameEvent.OnPlayerStopJump -= StopJump;
        }

        protected override void InnerUpdate()
        {
            base.InnerUpdate();

            if (!stat.IsAlive)
            {
                return;
            }

            if (!_renderer.IsVisibleCamera())
            {
                Death();
                return;
            }

            if (stat.IsCharacterState(CharacterState.Move))
            {
                var dir = _direction * (stat.moveSpeed * Time.fixedDeltaTime);
                dir.y = rb.velocity.y;
                rb.velocity = dir;
            }

            var size = Physics2D.RaycastNonAlloc(transform.position, Vector2.down * distance, _hit2Ds, distance);
            var ground = false;

            if (size <= 0)
            {
                if (stat.IsCharacterState(CharacterState.IdleOrRespawn))
                {
                    ChangeAnimation(3);
                }

                return;
            }

            for (var i = 0; i < size; i++)
            {
                switch (_hit2Ds[i].collider.tag)
                {
                    case "Ground":
                    case "BoxGold":
					case "Platform":
                        if (!stat.IsCharacterState(CharacterState.Move))
                        {
                            stat.IsCharacterState(CharacterState.IdleOrRespawn);
                            ChangeAnimation(0);
                            rb.ResetInertia();
                        }

						if (_hit2Ds[i].collider.name.Equals("Platform"))
						{
							transform.SetParent(_hit2Ds[i].collider.transform);
						}

                        ground = true;
                        _isJump = true;
                        _isFalling = false;
                        _isDoubleJump = false;
                        break;

                    default:
                        if (!ground && stat.IsCharacterState(CharacterState.IdleOrRespawn))
                        {
                            ChangeAnimation(3);
                        }

                        break;
                }
            }
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            switch (other.collider.tag)
            {
                case "Monster":
                    var monster = other.collider.GetComponent<Monster.Monster>();

                    if (!monster.stat.IsAlive)
                    {
                        return;
                    }

                    if (other.contacts[0].normal.y <= 0)
                    {
                        Death();
                    }
                    else
                    {
                        monster.Hurt();
                    }

                    break;

                case "CheckPoint":
                    CombatManager.Instance.CreateToast(other.collider.transform, "Check point");
                    _checkPoint = other.collider.transform.position;
                    other.collider.gameObject.SetActive(false);
                    break;

                case "Coin":
                    UIManager.Instance.AddScore(other.collider.transform, 100);
                    UserDataManager.Instance.AddMoney(100);
                    AudioManager.Instance.PlaySoundLoop("Sound Coin");
                    other.collider.gameObject.SetActive(false);
                    break;

                case "BoxGold":
                    if (transform.position.y < other.collider.transform.position.y)
                    {
                        other.collider.GetComponent<BoxGold>()?.Break();
                    }

                    break;

                case "Castle":
                    if (stat.IsAlive)
                    {
                        GameManager.GameManager.Instance.OnWin();
                    }

                    break;
            }
        }

        private void OnCollisionStay2D(Collision2D other)
        {
            if (!other.collider.CompareTag("Monster"))
            {
                return;
            }

            var monster = other.collider.GetComponent<Monster.Monster>();

            if (!monster.stat.IsAlive)
            {
                return;
            }

            if (other.contacts[0].normal.y <= 0)
            {
                Death();
            }
            else
            {
                monster.Hurt();
            }
        }
    }
}