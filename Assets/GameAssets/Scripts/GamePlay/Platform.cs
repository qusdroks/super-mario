using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay
{
	public class Platform : MonoBehaviour
	{
		[SerializeField] private float minX;
		[SerializeField] private float maxX;
		
		[SerializeField] private float minY;
		[SerializeField] private float maxY;
		
		[SerializeField] private float moveSpeed = 1.25f;
		
		[SerializeField] private bool vertical;
		
		private void LateUpdate()
		{
			transform.Translate((vertical ? Vector2.up : Vector2.right) * moveSpeed * Time.fixedDeltaTime);
			
			if (vertical)
			{
				if (transform.localPosition.y <= minY && moveSpeed < 0 ||
					transform.localPosition.y >= maxY && moveSpeed > 0)
				{
					moveSpeed *= -1;
				}
			}
			else
			{
				if (transform.localPosition.x <= minX && moveSpeed < 0 ||
					transform.localPosition.x >= maxX && moveSpeed > 0)
				{
					moveSpeed *= -1;
				}
			}
		}
	}
}
