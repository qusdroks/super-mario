using GameAssets.Scripts.GameManager;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using SceneManager = UnityEngine.SceneManagement.SceneManager;

namespace GameAssets.Scripts.GamePlay
{
    public class Level : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI txtLevel;
        [SerializeField] private Button btnLevel;
        [SerializeField] private Image imgLock;

        public void Init(int level, bool unlock)
        {
            if (unlock)
            {
                imgLock.gameObject.SetActive(false);
                txtLevel.gameObject.SetActive(true);
                txtLevel.text = $"{level}";

                btnLevel.onClick.AddListener(() =>
                {
                    UserDataManager.Instance.SelectLevel(level - 1);
                    SceneManager.LoadSceneAsync("Game Play");
                });
            }
            else
            {
                btnLevel.interactable = false;
            }
        }
    }
}