using System.Collections;
using GameAssets.Scripts.GameBase;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Monster
{
    public sealed partial class Monster : BaseCharacter
    {
        [SerializeField] private new SpriteRenderer renderer;
        [SerializeField] private Transform down;

        [SerializeField] private int score;

        [SerializeField] private float minX;
        [SerializeField] private float maxX;
        [SerializeField] private float lerpSpeed;

        private void Move()
        {
            transform.Translate(Vector3.right * (stat.moveSpeed * Time.fixedDeltaTime));

            if (transform.localPosition.x <= minX && stat.moveSpeed < 0 ||
                transform.localPosition.x >= maxX && stat.moveSpeed > 0)
            {
                Flip();
            }
        }

        private void Flip()
        {
            stat.moveSpeed *= -1;
            renderer.flipX = stat.moveSpeed > 0;
        }

        private static IEnumerator FollowArc(Transform mover, Vector2 start, Vector2 end, float radius, float duration)
        {
            var difference = end - start;
            var span = difference.magnitude;

            // Override the radius if it's too small to bridge the points.
            var absRadius = Mathf.Abs(radius);

            if (span > 2f * absRadius)
            {
                radius = absRadius = span / 2f;
            }

            var perpendicular = new Vector2(difference.y, -difference.x) / span;
            perpendicular *= Mathf.Sign(radius) * Mathf.Sqrt(radius * radius - span * span / 4f);

            var center = start + difference / 2f + perpendicular;
            var toStart = start - center;
            var startAngle = Mathf.Atan2(toStart.y, toStart.x);

            var toEnd = end - center;
            var endAngle = Mathf.Atan2(toEnd.y, toEnd.x);

            // Choose the smaller of two angles separating the start & end
            var travel = (endAngle - startAngle + 5f * Mathf.PI) % (2f * Mathf.PI) - Mathf.PI;
            var progress = 0f;

            do
            {
                var angle = startAngle + progress * travel;
                mover.position = center + new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * absRadius;
                progress += Time.deltaTime / duration;
                yield return null;
            } while (progress < 1f);

            mover.position = end;
        }
    }
}