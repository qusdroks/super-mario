using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Monster
{
    public sealed partial class Monster
    {
        protected override void InnerFixedUpdate()
        {
            base.InnerFixedUpdate();
            Move();
        }
    }
}