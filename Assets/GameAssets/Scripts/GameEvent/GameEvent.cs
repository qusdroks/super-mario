﻿using UnityEngine;

namespace GameAssets.Scripts.GameEvent
{
    public class GameEvent : MonoBehaviour
    {
        #region Events

        // Main events
        public delegate void TestWin();

        public delegate void TestLose();

        public event TestWin OnTestWin;
        public event TestLose OnTestLose;

        // Game events
        public delegate void PlayerShoot();

        public delegate void PlayerJump();

        public delegate void PlayerMoveLeft();

        public delegate void PlayerMoveRight();

        public delegate void PlayerStopMove();

        public delegate void PlayerStopJump();

        public static event PlayerShoot OnPlayerShoot;
        public static event PlayerJump OnPlayerJump;
        public static event PlayerMoveLeft OnPlayerMoveLeft;
        public static event PlayerMoveRight OnPlayerMoveRight;
        public static event PlayerStopMove OnPlayerStopMove;
        public static event PlayerStopJump OnPlayerStopJump;

        #endregion

        #region Main events

        private void Awake()
        {
            OnTestWin += TestWinHandle;
            OnTestLose += TestLoseHandle;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                OnTestWin?.Invoke();
            }

            if (Input.GetKeyDown(KeyCode.L))
            {
                OnTestLose?.Invoke();
            }
        }

        private void OnDestroy()
        {
            OnTestWin -= TestWinHandle;
            OnTestLose -= TestLoseHandle;
        }

        private static void TestWinHandle()
        {
            GameManager.GameManager.Instance.OnWin();
        }

        private static void TestLoseHandle()
        {
            GameManager.GameManager.Instance.OnLose();
        }

        #endregion

        public static void DoPlayerShoot() => OnPlayerShoot?.Invoke();
        public static void DoPlayerJump() => OnPlayerJump?.Invoke();
        public static void DoPlayerMoveLeft() => OnPlayerMoveLeft?.Invoke();
        public static void DoPlayerMoveRight() => OnPlayerMoveRight?.Invoke();
        public static void DoPlayerStopMove() => OnPlayerStopMove?.Invoke();
        public static void DoPlayerStopJump() => OnPlayerStopJump?.Invoke();
    }
}