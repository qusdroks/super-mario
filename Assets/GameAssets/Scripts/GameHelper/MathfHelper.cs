﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.General;
using UnityEngine;
using SRandom = System.Random;
using URandom = UnityEngine.Random;

namespace GameAssets.Scripts.GameHelper
{
    public static class MathfHelper
    {
        #region Camera

        public static float Left(this Camera camera)
        {
            return camera.ViewportToWorldPoint(Vector3.zero).x;
        }

        public static float Right(this Camera camera)
        {
            return camera.ViewportToWorldPoint(Vector3.right).x;
        }

        public static float Top(this Camera camera)
        {
            return camera.ViewportToWorldPoint(Vector3.up).y;
        }

        public static float Bottom(this Camera camera)
        {
            return camera.ViewportToWorldPoint(Vector3.zero).y;
        }

        public static Vector3 Center(this Camera camera)
        {
            return camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0f));
        }

        public static bool ScreenContainsPoint(this Camera camera, Vector3 worldPosition)
        {
            return camera.rect.Contains(camera.WorldToViewportPoint(worldPosition));
        }

        public static void ConstraintCamera(this Camera camera, Bounds bounds)
        {
            var left = camera.ViewportToWorldPoint(Vector3.zero).x;
            var right = camera.ViewportToWorldPoint(Vector3.right).x;
            var top = camera.ViewportToWorldPoint(Vector3.up).y;
            var bottom = camera.ViewportToWorldPoint(Vector3.zero).y;

            if (top > bounds.max.y)
            {
                var topDiff = bounds.max.y - top;
                camera.transform.position += new Vector3(0, topDiff, 0);
            }
            else if (bottom < bounds.min.y)
            {
                var botDiff = bounds.min.y - bottom;
                camera.transform.position += new Vector3(0, botDiff, 0);
            }

            if (right > bounds.max.x)
            {
                var rightDiff = bounds.max.x - right;
                camera.transform.position += new Vector3(rightDiff, 0, 0);
            }
            else if (left < bounds.min.x)
            {
                var leftDiff = bounds.min.x - left;
                camera.transform.position += new Vector3(leftDiff, 0, 0);
            }
        }

        #endregion

        #region Void

        public static void MoveLerp(this Component t, Vector3 target, float moveSpeed, bool changeToTime = true)
        {
            t.transform.position = Vector3.Lerp(t.transform.position, target,
                moveSpeed * (changeToTime ? Time.fixedDeltaTime : 1f));
        }

        public static void LimitCamera(this Transform origin)
        {
            var pos = GameManager.GameManager.Instance.Camera.WorldToViewportPoint(origin.position);

            pos.x = Mathf.Clamp01(pos.x);
            pos.y = Mathf.Clamp01(pos.y);

            origin.position = GameManager.GameManager.Instance.Camera.ViewportToWorldPoint(pos);
        }

        public static void SetPosition(this Transform t, Transform target)
        {
            t.position = target.position;
        }

        public static void SetPosition(this Transform t, Vector3 target)
        {
            t.position = target;
        }

        public static void SetLocalPosition(this Transform t, Transform target)
        {
            t.localPosition = target.position;
        }

        public static void SetLocalPosition(this Transform t, Vector3 target)
        {
            t.localPosition = target;
        }

        public static void SetAnchoredPosition(this RectTransform t, Vector3 target)
        {
            t.anchoredPosition = target;
        }

        #endregion

        #region Quaternion

        public static Quaternion ToQuaternion(this Vector3 v)
        {
            var angle = Mathf.Atan2(v.x, v.z);
            return new Quaternion(v.x * Mathf.Sin(angle / 2), v.y * Mathf.Sin(angle / 2), v.z * Mathf.Sin(angle / 2),
                Mathf.Cos(angle / 2));
        }

        #endregion

        #region Vector3

        public static Vector3 RandomRadius(this Vector3 original, float radius)
        {
            return Vector3.ProjectOnPlane(URandom.insideUnitSphere, original).normalized * radius;
        }

        public static Vector3 Random(this Bounds bounds, float scale = 1f)
        {
            var min = bounds.min;
            var max = bounds.max;
            return new Vector3(URandom.Range(min.x * scale, max.x * scale),
                URandom.Range(min.y * scale, max.y * scale),
                URandom.Range(min.z * scale, max.z * scale));
        }

        public static Vector3 GetPlatformPosition =>
            Application.platform switch
            {
                RuntimePlatform.WindowsEditor => Input.mousePosition,
                RuntimePlatform.WindowsPlayer => Input.mousePosition,
                RuntimePlatform.Android => Input.touchCount > 0
                    ? (Vector3) Input.GetTouch(0).position
                    : Vector3.zero,
                RuntimePlatform.IPhonePlayer => Input.touchCount > 0
                    ? (Vector3) Input.GetTouch(0).position
                    : Vector3.zero,
                _ => Vector3.zero
            };

        public static Vector3 Abs(this Vector3 v)
        {
            return new Vector3(Mathf.Abs(v.x), Mathf.Abs(v.y), Mathf.Abs(v.z));
        }

        public static Vector3 GetDirection(this Vector3 v)
        {
            return XYZDirection(v) switch
            {
                0 => Vector3.right,
                1 => Vector3.up,
                2 => Vector3.forward,
                _ => throw new InvalidOperationException()
            };
        }

        public static Vector3 GetDirectionFromInt(this int i)
        {
            return i switch
            {
                0 => Vector3.right,
                1 => Vector3.up,
                2 => Vector3.forward,
                _ => throw new InvalidOperationException()
            };
        }

        #endregion

        #region Int

        public static int XYZDirection(this Vector3 v)
        {
            var x = Mathf.Abs(v.x);
            var y = Mathf.Abs(v.y);
            var z = Mathf.Abs(v.z);
            return x > y & x > z ? 0 : y > x & y > z ? 1 : 2;
        }

        public static int? GetIndexOfLowestLayer(this int layer)
        {
            for (var index = 0; index < sizeof(int) * 8; index++)
            {
                if ((layer & 1) == 1)
                {
                    return index;
                }

                layer >>= 1;
            }

            return null;
        }

        #endregion

        #region Float

        public static float MaxAbs(this float a, float b)
        {
            return Mathf.Abs(a) > Mathf.Abs(b) ? a : b;
        }

        public static float NextFloat(this SRandom rnd, float max, float min)
        {
            if (min > max)
            {
                return (float) rnd.NextDouble() * (min - max) + max;
            }

            return (float) rnd.NextDouble() * (max - min) + min;
        }

        public static float RemapNumber(this float n, float l1, float h1, float l2, float h2)
        {
            return l2 + (n - l1) * (h2 - l2) / (h1 - l1);
        }

        public static float RemapNumberClamped(this float n, float l1, float h1, float l2, float h2)
        {
            return Mathf.Clamp(RemapNumber(n, l1, h1, l2, h2), Mathf.Min(l2, h2),
                Mathf.Max(l2, h2));
        }

        public static float DropChance(this List<float> drops)
        {
            var range = drops.Sum();
            var rnd = URandom.Range(0, range);
            var top = 0f;

            foreach (var x in drops)
            {
                top += x;

                if (rnd < top)
                {
                    return x;
                }
            }

            return drops[0];
        }

        #endregion

        #region Double

        public static double AngleBetween(this Transform t1, Transform t2)
        {
            var v1 = t1.position;
            var v2 = t2.position;

            var sin = v1.x * v2.y - v2.x * v1.y;
            var cos = v1.x * v2.x + v1.y * v2.y;
            return Math.Atan2(sin, cos) * (180 / Math.PI);
        }

        public static double NextDouble(this SRandom rnd, double max, double min)
        {
            if (min > max)
            {
                return rnd.NextDouble() * (min - max) + max;
            }

            return rnd.NextDouble() * (max - min) + min;
        }

        #endregion

        #region String

        public static string Minimum10(this float value)
        {
            return value < 10 ? $"0{value}" : $"{value}";
        }

        public static string Minimum10(this int value)
        {
            return value < 10 ? $"0{value}" : $"{value}";
        }

        public static string Minimum10(this double value)
        {
            return value < 10 ? $"0{value}" : $"{value}";
        }

        public static string Match(this string s, char c1, char c2 = '\0', bool isFirstLast = false)
        {
            var begin = isFirstLast ? s.LastIndexOf(c1) : s.IndexOf(c1);
            var end = c2 == '\0' ? s.Length : s.LastIndexOf(c2);
            return s.Substring(begin + 1, end - begin - 1);
        }

        #endregion

        #region Bool

        public static bool NearlyEqual(this float a, float b, float epsilon = 1f)
        {
            var absA = Math.Abs(a);
            var absB = Math.Abs(b);
            var diff = Math.Abs(a - b);

            if (a.Equals(b))
            {
                return true;
            }

            if (a.Equals(0) || b.Equals(0) || absA + absB < Const.MIN_NORMAL)
            {
                return diff < epsilon * Const.MIN_NORMAL;
            }

            return diff / (absA + absB) < epsilon;
        }

        public static bool NextBool => URandom.value > 0.5f;

        public static bool Distance(this Component a, Component b, float minDistance)
        {
            return (a.transform.position - b.transform.position).sqrMagnitude < minDistance * minDistance;
        }
        
        #endregion
    }
}