﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using GameAssets.Scripts.GameBase;
using TMPro;
using UnityEngine;

namespace GameAssets.Scripts.GameHelper
{
    public class SpawnerHelper : BaseSingleton<SpawnerHelper>
    {
        [SerializeField] private GameObject toastPrefab;

        private static readonly Dictionary<string, Queue<GameObject>> Pooling =
            new Dictionary<string, Queue<GameObject>>();

        private static GameObject _toast;

        private GameObject CreateToast()
        {
            return CreateSpawner(Vector3.zero, null, toastPrefab);
        }

        private static GameObject GetNormalObject(GameObject go, Transform parent)
        {
            if (!Pooling.TryGetValue(go.name, out var queue))
            {
                return CreateNormalObject(go, parent);
            }

            if (queue.Count == 0)
            {
                return CreateNormalObject(go, parent);
            }

            var clone = queue.Dequeue();

            if (clone == null)
            {
                clone = CreateNormalObject(go, parent);
            }

            clone.SetActive(true);
            return clone;
        }

        private static GameObject GetUIObject(GameObject go, Transform parent)
        {
            if (!Pooling.TryGetValue(go.name, out var queue))
            {
                return CreateUIObject(go, parent);
            }

            if (queue.Count == 0)
            {
                return CreateUIObject(go, parent);
            }

            var clone = queue.Dequeue();

            if (clone == null)
            {
                clone = CreateUIObject(go, parent);
            }

            clone.SetActive(true);
            return clone;
        }

        private static GameObject CreateNormalObject(GameObject go, Transform parent)
        {
            var clone = Instantiate(go, Vector3.zero, Quaternion.identity, parent);
            clone.name = go.name;
            return clone;
        }

        private static GameObject CreateUIObject(GameObject go, Transform parent)
        {
            var clone = Instantiate(go, parent);
            clone.name = go.name;
            return clone;
        }

        private static async void DestroyObject(GameObject go, float timeDestroy)
        {
            await UniTask.Delay(TimeSpan.FromSeconds(timeDestroy));

            if (go == null)
            {
                return;
            }

            if (Pooling.TryGetValue(go.name, out var queue))
            {
                queue.Enqueue(go);
            }
            else
            {
                var newQueue = new Queue<GameObject>();
                newQueue.Enqueue(go);
                Pooling.Add(go.name, newQueue);
            }

            go.SetActive(false);
        }

        public static GameObject CreateSpawner(Vector3 position, Transform parent, GameObject prefab,
            bool isSetLocal = false)
        {
            var go = GetNormalObject(prefab, parent);

            if (isSetLocal)
            {
                go.transform.localPosition = position;
            }
            else
            {
                go.transform.position = position;
            }

            return go;
        }

        public static GameObject CreateUISpawner(Transform parent, GameObject prefab)
        {
            return GetUIObject(prefab, parent);
        }

        public static void DestroySpawner(GameObject go, float timeDestroy = 0f)
        {
            DestroyObject(go, timeDestroy);
        }

        public static GameObject CreateAndDestroy(Vector3 position, Transform parent, GameObject prefab,
            bool isSetLocal = false, float timeDestroy = 0f)
        {
            var clone = CreateSpawner(position, parent, prefab, isSetLocal);
            DestroySpawner(clone, timeDestroy);
            return clone;
        }

        public static GameObject CreateAndDestroyUI(Transform parent, GameObject prefab, float timeDestroy = 0f)
        {
            var clone = CreateUISpawner(parent, prefab);
            DestroySpawner(clone, timeDestroy);
            return clone;
        }

        public static void CreateToast(string msg, float timeDestroy = 1.5f)
        {
            if (_toast != null)
            {
                DestroySpawner(_toast, timeDestroy);
            }

            _toast = Instance.CreateToast();
            var container = _toast.GetComponentInChildren<RectTransform>();
            var content = _toast.GetComponentInChildren<TextMeshProUGUI>();

            content.text = msg;
            SetPosition(container);
            DestroySpawner(_toast, timeDestroy);
        }

        private static void SetPosition(RectTransform rt)
        {
            rt.anchorMin = new Vector2(0.5f, 0);
            rt.anchorMax = new Vector2(0.5f, 0);
            rt.anchoredPosition = new Vector3(0.5f, 100f, 0);
        }
    }
}