﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameHelper
{
    public static class UIHelper
    {
        private static readonly int MainTex = Shader.PropertyToID("_MainTex");
        private static readonly int Progress = Shader.PropertyToID("_Progress");

        public static async void SceneTransition(this Image img, Sprite transition, float lerpSpeed = 1f,
            Action onTransitionStarted = null, Action onTransitionCompleted = null)
        {
            onTransitionStarted?.Invoke();
            img.material.SetTexture(MainTex, transition.texture);

            while (img.material.GetFloat(Progress) < 1.1f)
            {
                img.material.SetFloat(Progress, Mathf.MoveTowards(img.material.GetFloat(Progress),
                    1.1f, lerpSpeed * Time.deltaTime));
                await UniTask.Yield();
            }

            onTransitionCompleted?.Invoke();
        }

        public static void OpenClose(this CanvasGroup cg)
        {
            cg.alpha = cg.alpha > 0 ? 0 : 1;
            cg.blocksRaycasts = cg.blocksRaycasts != true;
        }

        public static void SetActive(this ParticleSystem ps, bool active = true)
        {
            if (active)
            {
                ps.gameObject.SetActive(true);
                ps.Play();
            }
            else
            {
                var main = ps.main;
                main.stopAction = ParticleSystemStopAction.Disable;

                ps.IgnoreRaycast();
                ps.Stop();
            }
        }

        public static void IgnoreRaycast(this Component c)
        {
            c.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
        }

        private static string GetHtmlFromUri(string resource)
        {
            var html = string.Empty;
            var req = (HttpWebRequest) WebRequest.Create(resource);

            try
            {
                using var resp = (HttpWebResponse) req.GetResponse();
                var success = (int) resp.StatusCode < 299 && (int) resp.StatusCode >= 200;

                if (success)
                {
                    using var reader =
                        new StreamReader(resp.GetResponseStream() ?? throw new InvalidOperationException());
                    //We are limiting the array to 80 so we don't have
                    //to parse the entire html document feel free to 
                    //adjust (probably stay under 300)
                    var cs = new char[80];
                    reader.Read(cs, 0, cs.Length);
                    html = cs.Aggregate(html, (current, ch) => current + ch);
                }
            }
            catch
            {
                return "";
            }

            return html;
        }

        public static bool NetworkEvent(Action onDisconnect = null, Action onConnected = null)
        {
            var html = GetHtmlFromUri("https://google.com");

            if (html == "")
            {
                // No connection
                onDisconnect?.Invoke();
                return false;
            }

            if (!html.Contains("schema.org/WebPage"))
            {
                // Redirecting since the beginning of googles html contains that 
                // phrase and it was not found
            }
            else
            {
                // success
                onConnected?.Invoke();
                return true;
            }

            return false;
        }

        public static Rect GetWorldRect(this RectTransform rt, Vector3 scale = default)
        {
            var corners = new Vector3[4];
            rt.GetWorldCorners(corners);

            var bottomLeft = corners[0];
            var rect = rt.rect;

            if (scale.Equals(default))
            {
                scale = Vector3.zero;
            }

            var scaledSize = new Vector2(scale.x * rect.size.x, scale.y * rect.size.y);
            return new Rect(bottomLeft, scaledSize);
        }

        public static bool IsVisibleCamera(this Renderer renderer)
        {
            var planes = GeometryUtility.CalculateFrustumPlanes(GameManager.GameManager.Instance.Camera);
            return GeometryUtility.TestPlanesAABB(planes, renderer.bounds);
        }

        public static void SetLeftRight(this RectTransform rt, float left, float right)
        {
            var min = rt.offsetMin;
            min += new Vector2(left - min.x, 0f);
            rt.offsetMin = min;

            var max = rt.offsetMax;
            max -= new Vector2(right + max.x, 0f);
            rt.offsetMax = max;
        }
    }
}