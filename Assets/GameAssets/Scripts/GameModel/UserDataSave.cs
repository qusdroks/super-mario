﻿using System;
using System.Collections.Generic;
using GameAssets.Scripts.GameHelper;

namespace GameAssets.Scripts.GameModel
{
    [Serializable]
    public class UserDataSave
    {
        public int level;
        public int selectLevel;

        public float diamond;
        public float money;
        public float death;

        public float hours;
        public float minutes;
        public float seconds;

        public bool sound;
        public bool music;
        public bool vibration;
        public bool removedAds;

        public List<Highscore> highscores = new List<Highscore>();
        public List<Ability> abilities = new List<Ability>();

        public string GetTimePlayed => $"{hours.Minimum10()}:{minutes.Minimum10()}:{seconds.Minimum10()}";
    }
}