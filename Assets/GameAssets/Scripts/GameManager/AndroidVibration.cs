﻿/*using System;
using System.Collections;
using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.GameManager
{
    public static class AndroidVibration
    {
        private static int _sdkVersion = -1;

        private static readonly long[] SuccessPattern =
        {
            0, Const.LIGHT_DURATION,
            Const.LIGHT_DURATION,
            Const.HEAVY_DURATION
        };

        private static readonly int[] SuccessPatternAmplitude =
        {
            0, Const.LIGHT_AMPLITUDE,
            0, Const.HEAVY_AMPLITUDE
        };

        private static readonly long[] WarningPattern =
        {
            0, Const.HEAVY_DURATION,
            Const.LIGHT_DURATION,
            Const.MEDIUM_DURATION
        };

        private static readonly int[] WarningPatternAmplitude =
        {
            0, Const.HEAVY_AMPLITUDE,
            0, Const.MEDIUM_AMPLITUDE
        };

        private static readonly long[] FailurePattern =
        {
            0, Const.MEDIUM_DURATION,
            Const.LIGHT_DURATION, Const.MEDIUM_DURATION,
            Const.LIGHT_DURATION, Const.HEAVY_DURATION,
            Const.LIGHT_DURATION, Const.LIGHT_DURATION
        };

        private static readonly int[] FailurePatternAmplitude =
        {
            0, Const.MEDIUM_AMPLITUDE,
            0, Const.MEDIUM_AMPLITUDE,
            0, Const.HEAVY_AMPLITUDE,
            0, Const.LIGHT_AMPLITUDE
        };

        private static void VibrateHandheld()
        {
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                case RuntimePlatform.IPhonePlayer:
                    Handheld.Vibrate();
                    break;
            }
        }

        public static void Vibrate()
        {
            AndroidVibrate(Const.MEDIUM_DURATION);
        }

        public static void Haptic(VibrationType type)
        {
            switch (type)
            {
                case VibrationType.Selection:
                    AndroidVibrate(Const.LIGHT_DURATION, Const.LIGHT_AMPLITUDE);
                    break;

                case VibrationType.Success:
                    AndroidVibrate(SuccessPattern, SuccessPatternAmplitude, -1);
                    break;

                case VibrationType.Warning:
                    AndroidVibrate(WarningPattern, WarningPatternAmplitude, -1);
                    break;

                case VibrationType.Failure:
                    AndroidVibrate(FailurePattern, FailurePatternAmplitude, -1);
                    break;

                case VibrationType.LightImpact:
                    AndroidVibrate(Const.LIGHT_DURATION, Const.LIGHT_AMPLITUDE);
                    break;

                case VibrationType.MediumImpact:
                    AndroidVibrate(Const.MEDIUM_DURATION, Const.MEDIUM_AMPLITUDE);
                    break;

                case VibrationType.HeavyImpact:
                    AndroidVibrate(Const.HEAVY_DURATION, Const.HEAVY_AMPLITUDE);
                    break;
            }
        }

#if UNITY_IOS && !UNITY_EDITOR
        private static readonly AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        private static readonly AndroidJavaObject CurrentActivity =
 UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        private static readonly AndroidJavaObject AndroidVibrator =
            CurrentActivity.Call<AndroidJavaObject>("getSystemService", "vibrator");
#else
        private static readonly AndroidJavaObject AndroidVibrator = null;
        private static AndroidJavaClass _vibrationEffectClass;
        private static AndroidJavaObject _vibrationEffect;
#endif

        private static void AndroidVibrate(long milliseconds)
        {
            AndroidVibrator.Call("vibrate", milliseconds);
        }

        private static void AndroidVibrate(long milliseconds, int amplitude)
        {
            if (AndroidSDKVersion() < 26)
            {
                AndroidVibrate(milliseconds);
            }
            else
            {
                VibrationEffectClassInitialization();
                _vibrationEffect =
                    _vibrationEffectClass.CallStatic<AndroidJavaObject>("createOneShot", milliseconds, amplitude);
                AndroidVibrator.Call("vibrate", _vibrationEffect);
            }
        }

        public static void AndroidVibrate(long[] pattern, int repeat)
        {
            if (AndroidSDKVersion() < 26)
            {
                AndroidVibrator.Call("vibrate", pattern, repeat);
            }
            else
            {
                VibrationEffectClassInitialization();
                _vibrationEffect =
                    _vibrationEffectClass.CallStatic<AndroidJavaObject>("createWaveform", pattern, repeat);
                AndroidVibrator.Call("vibrate", _vibrationEffect);
            }
        }

        private static void AndroidVibrate(IEnumerable pattern, IEnumerable amplitudes, int repeat)
        {
            if (AndroidSDKVersion() < 26)
            {
                AndroidVibrator.Call("vibrate", pattern, repeat);
            }
            else
            {
                VibrationEffectClassInitialization();
                _vibrationEffect =
                    _vibrationEffectClass.CallStatic<AndroidJavaObject>("createWaveform", pattern, amplitudes, repeat);
                AndroidVibrator.Call("vibrate", _vibrationEffect);
            }
        }

        public static void AndroidCancelVibrations()
        {
            AndroidVibrator.Call("cancel");
        }

        private static void VibrationEffectClassInitialization()
        {
            if (_vibrationEffectClass != null)
            {
                return;
            }

            _vibrationEffectClass = new AndroidJavaClass("android.os.VibrationEffect");
        }

        private static int AndroidSDKVersion()
        {
            if (_sdkVersion != -1)
            {
                return _sdkVersion;
            }

            var apiLevel =
                int.Parse(SystemInfo.operatingSystem.Substring(
                    SystemInfo.operatingSystem.IndexOf("-", StringComparison.Ordinal) + 1, 3));

            _sdkVersion = apiLevel;
            return apiLevel;
        }
    }
}*/