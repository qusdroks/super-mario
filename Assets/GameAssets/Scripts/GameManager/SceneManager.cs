namespace GameAssets.Scripts.GameManager
{
    public static class SceneManager
    {
        public static void ResetCurrentScene()
        {
            UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(UnityEngine.SceneManagement.SceneManager
                .GetActiveScene().name);
        }
    }
}