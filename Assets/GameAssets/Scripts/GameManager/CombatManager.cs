﻿using DG.Tweening;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using TMPro;
using UnityEngine;

namespace GameAssets.Scripts.GameManager
{
    public class CombatManager : BaseSingleton<CombatManager>
    {
        [SerializeField] private GameObject combatPrefab;
        [SerializeField] private Canvas combatCanvas;
        [SerializeField] private float lerpSpeed = 0.5f;

        private Transform _parent;

        protected override void InitAwake()
        {
            base.InitAwake();
            _parent = combatCanvas.transform;
        }

        public void CreateToast(Transform target, string message)
        {
            var combat = SpawnerHelper.CreateSpawner(target.position, _parent, combatPrefab)
                .GetComponent<TextMeshProUGUI>();
            combat.color = Color.white;
            combat.text = message;

            var sequence = DOTween.Sequence();
            sequence.Join(combat.transform.DOMove(combat.transform.position + Vector3.up * 5f, lerpSpeed))
                .Join(combat.DOFade(0f, lerpSpeed))
                .Play().OnComplete(() => SpawnerHelper.DestroySpawner(combat.gameObject));
        }
    }
}