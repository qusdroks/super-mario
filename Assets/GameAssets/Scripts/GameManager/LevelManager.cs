﻿using System.Collections.Generic;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using UnityEngine;

namespace GameAssets.Scripts.GameManager
{
    public class LevelManager : BaseSingleton<LevelManager>
    {
        [SerializeField] private List<GameObject> allLevels = new List<GameObject>();
        [SerializeField] private Transform parent;
        [SerializeField] private bool isLoopLevel;
        [SerializeField] private bool isLoadLevel = true;

        public List<GameObject> AllLevels => allLevels;

        public int currentLevel;

        protected override void InitAwake()
        {
            base.InitAwake();
            currentLevel = UserDataManager.Instance.userDataSave.selectLevel;
            LoadLevel();
        }

        private void LoadLevel()
        {
            if (isLoadLevel && allLevels.Count <= 0 || !isLoadLevel)
            {
                return;
            }

            var index = isLoopLevel ? currentLevel % allLevels.Count :
                currentLevel > allLevels.Count - 1 ? allLevels.Count - 1 : currentLevel;
            SpawnerHelper.CreateSpawner(Vector3.zero, parent, allLevels[index]);
        }
    }
}