﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.General;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameManager
{
    public class UIManager : BaseSingleton<UIManager>
    {
        [SerializeField] private List<GameObject> uis = new List<GameObject>();
        [SerializeField] private TextMeshProUGUI txtTotalScore;
        [SerializeField] private TextMeshProUGUI txtAmountBullet;
        [SerializeField] private Image imgPause;
        [SerializeField] private Sprite spritePause;
        [SerializeField] private Sprite spriteUnpause;
        [SerializeField] private UIType uiAwake;

        [SerializeField] private int amountBullet = 3;

        private int _currentAmountBullet;
        private int _totalScore;

        protected override void InitAwake()
        {
            base.InitAwake();
            _currentAmountBullet = amountBullet;

            if (txtAmountBullet != null)
            {
                txtAmountBullet.text = $"{_currentAmountBullet}/{amountBullet}";
            }

            HideAllAndShowUI(uiAwake);
        }

        public void CanShoot(Action onShoot)
        {
            if (_currentAmountBullet <= 0)
            {
                return;
            }

            _currentAmountBullet--;
            txtAmountBullet.text = $"{_currentAmountBullet}/{amountBullet}";
            onShoot();
        }

        public void AddScore(Transform target, int score)
        {
            _totalScore += score;
            txtTotalScore.text = $"{_totalScore:D5}";
            CombatManager.Instance.CreateToast(target, $"+{score}");
        }

        public void HideAllAndShowUI(UIType type)
        {
            foreach (var ui in uis.Select(x => x.GetComponent<BaseUI>()))
            {
                ui.DoAnimation(ui.UIType.Equals(type));
            }
        }

        public void ShowUI<T>(Action onShowComplete = null) where T : BaseUI
        {
            var t = GetUI<T>();
            t.DoAnimation(true);
            onShowComplete?.Invoke();
        }

        public T GetUI<T>() where T : BaseUI
        {
            return uis.Select(x => x.GetComponent<T>()).FirstOrDefault(t => t != null);
        }

        public void ChangeSpritePause()
        {
            imgPause.sprite = imgPause.sprite.name.Equals(spritePause.name) ? spriteUnpause : spritePause;
        }
    }
}