﻿using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.GamePlay.Player;
using GameAssets.Scripts.GameUI.UIGameComplete;
using GameAssets.Scripts.GameUI.UIGameDraw;
using GameAssets.Scripts.GameUI.UIGameOver;
using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.GameManager
{
    [RequireComponent(typeof(SpawnerHelper))]
    public class GameManager : BaseSingleton<GameManager>
    {
        [SerializeField] private List<BaseCharacter> characters = new List<BaseCharacter>();
        [SerializeField] private new Camera camera;

        public Camera Camera => camera;

        public GameState currentState;

        protected override void InitAwake()
        {
            base.InitAwake();

            if (!Application.platform.Equals(RuntimePlatform.WebGLPlayer))
            {
                Application.targetFrameRate = 120;
            }

            QualitySettings.vSyncCount = 0;
            AddBaseCharacter(FindObjectsOfType<BaseCharacter>());
        }

        public void AddBaseCharacter(params BaseCharacter[] character)
        {
            foreach (var x in character)
            {
                if (!characters.Contains(x))
                {
                    characters.Add(x);
                }
            }
        }

        protected override void InnerUpdate()
        {
            base.InnerUpdate();

            foreach (var x in characters)
            {
                x.CurrentState?.UpdateState();
            }
        }

        public void SetGameState(GameState gameState)
        {
            if (Equals(currentState, gameState))
            {
                return;
            }

            currentState = gameState;
            Debug.Log($"GAME STATE ===> {gameState}");
        }

        public bool IsGameState(GameState state)
        {
            return currentState == state;
        }

        public void OnWin()
        {
            UIManager.Instance.ShowUI<UIGameComplete>();
            UserDataManager.Instance.LevelUp();
            AudioManager.Instance.PlaySoundLoop("Sound Win");

            SetGameState(GameState.Win);
            StopBaseCharacter();
        }

        public void OnDraw()
        {
            UIManager.Instance.ShowUI<UIGameDraw>();
            SetGameState(GameState.Draw);
            StopBaseCharacter();
        }

        public void OnLose()
        {
            UIManager.Instance.ShowUI<UIGameOver>();
            AudioManager.Instance.PlaySoundLoop("Sound Gameover");

            SetGameState(GameState.Lose);
            StopBaseCharacter();
        }

        public Vector3 CameraSceneToGame(Vector3 position)
        {
            return camera.ScreenToWorldPoint(position);
        }

        public Ray CameraSceneToRay(Vector3 position)
        {
            return camera.ScreenPointToRay(position);
        }

        private void StopBaseCharacter()
        {
            foreach (var x in characters.Where(x => x != null && x.stat.IsAlive))
            {
                if (x is Player)
                {
                    x.ChangeAnimation(0);
                }

                x.ChangeState();

                if (x.rb != null)
                {
                    x.rb.ResetInertia();
                }
            }
        }
    }
}