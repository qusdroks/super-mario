﻿namespace GameAssets.Scripts.General
{
    public enum VibrationType
    {
        Selection,
        Success,
        Warning,
        Failure,
        LightImpact,
        MediumImpact,
        HeavyImpact
    }

    public enum CharacterState
    {
        IdleOrRespawn = 0,
        Move = 1,
        Attack = 2,
        Death = 3
    }

    public enum GameState
    {
        Lobby,
        Loading,
        Pause,
        PreparePlay,
        Playing,
        Lose,
        Win,
        Draw
    }

    public enum UIType
    {
        None,
        UIPause,
        UILoading,
        UIPlay,
        UIGameStart,
        UIGameComplete,
        UIGameOver,
        UIGameDraw,
        UIWithdraw
    }

    public enum FitType
    {
        Uniform,
        Width,
        Height,
        FixedRows,
        FixedColumns,
        FixedBoth
    }

    public enum Alignment
    {
        Horizontal,
        Vertical
    }

    public enum PriorityLevel
    {
        Low,
        Normal,
        High
    }
}