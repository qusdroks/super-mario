﻿using GameAssets.Scripts.GameBase;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameUI.UILoading
{
    public partial class UILoading : BaseUI
    {
        [SerializeField] private Image imgLoading;
        [SerializeField] private float loadingTime = 5f;
        [SerializeField] private float loadingDuration = 0.25f;
    }
}
