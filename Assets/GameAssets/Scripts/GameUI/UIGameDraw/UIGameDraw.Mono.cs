using GameAssets.Scripts.GameManager;

namespace GameAssets.Scripts.GameUI.UIGameDraw
{
    public partial class UIGameDraw
    {
        private void Awake()
        {
            btnGameDraw.onClick.AddListener(SceneManager.ResetCurrentScene);
        }
    }
}