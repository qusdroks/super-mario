using GameAssets.Scripts.GameBase;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameUI.UIGameOver
{
    public partial class UIGameOver : BaseUI
    {
        [SerializeField] private Button btnGameOver;
        [SerializeField] private Button btnHome;
    }
}