using SceneManager = GameAssets.Scripts.GameManager.SceneManager;

namespace GameAssets.Scripts.GameUI.UIGameOver
{
    public partial class UIGameOver
    {
        private void Awake()
        {
            btnGameOver.onClick.AddListener(SceneManager.ResetCurrentScene);
            btnHome.onClick.AddListener(() => UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("Game Menu"));
        }
    }
}