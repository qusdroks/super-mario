using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.GameManager;
using GameAssets.Scripts.General;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameAssets.Scripts.GameUI.UIPlay
{
    public partial class UIPlay
    {
        private static readonly int Start = Animator.StringToHash("Start");

        private void Awake()
        {
            if (btnPlay != null)
            {
                btnPlay.onClick.AddListener(() => UIManager.Instance.ShowUI<UIGameStart.UIGameStart>());
            }

            if (btnPause != null)
            {
                btnPause.onClick.AddListener(() =>
                {
                    GameManager.GameManager.Instance.SetGameState(GameState.Pause);
                    UIManager.Instance.ShowUI<UIPause.UIPause>();
                    UIManager.Instance.ChangeSpritePause();
                });
            }

            if (btnWithdraw != null)
            {
                btnWithdraw.onClick.AddListener(() =>
                {
                    UIManager.Instance.HideAllAndShowUI(UIType.UIWithdraw);
                });
            }

            if (btnShoot == null)
            {
                return;
            }

            startAnimator.SetBool(Start, true);
            startAnimator.OnComplete(() => GameManager.GameManager.Instance.SetGameState(GameState.Playing));

            btnShoot.onClick.AddListener(GameEvent.GameEvent.DoPlayerShoot);

            var entry1 = new EventTrigger.Entry {eventID = EventTriggerType.PointerDown};
            entry1.callback.AddListener(_ => GameEvent.GameEvent.DoPlayerJump());
            btnJump.triggers.Add(entry1);
            
            var entry2 = new EventTrigger.Entry {eventID = EventTriggerType.PointerUp};
            entry2.callback.AddListener(_ => GameEvent.GameEvent.DoPlayerStopJump());
            btnJump.triggers.Add(entry2);
            
            var entry3 = new EventTrigger.Entry {eventID = EventTriggerType.PointerExit};
            entry3.callback.AddListener(_ => GameEvent.GameEvent.DoPlayerStopJump());
            btnJump.triggers.Add(entry3);
            
            var entry4 = new EventTrigger.Entry {eventID = EventTriggerType.PointerDown};
            entry4.callback.AddListener(_ => GameEvent.GameEvent.DoPlayerMoveLeft());
            btnMoveLeft.triggers.Add(entry4);
            
            var entry5 = new EventTrigger.Entry {eventID = EventTriggerType.PointerUp};
            entry5.callback.AddListener(_ => GameEvent.GameEvent.DoPlayerStopMove());
            btnMoveLeft.triggers.Add(entry5);
            
            var entry6 = new EventTrigger.Entry {eventID = EventTriggerType.PointerExit};
            entry6.callback.AddListener(_ => GameEvent.GameEvent.DoPlayerStopMove());
            btnMoveLeft.triggers.Add(entry6);
            
            var entry7 = new EventTrigger.Entry {eventID = EventTriggerType.PointerDown};
            entry7.callback.AddListener(_ => GameEvent.GameEvent.DoPlayerMoveRight());
            btnMoveRight.triggers.Add(entry7);
            
            var entry8 = new EventTrigger.Entry {eventID = EventTriggerType.PointerUp};
            entry8.callback.AddListener(_ => GameEvent.GameEvent.DoPlayerStopMove());
            btnMoveRight.triggers.Add(entry8);
            
            var entry9 = new EventTrigger.Entry {eventID = EventTriggerType.PointerExit};
            entry9.callback.AddListener(_ => GameEvent.GameEvent.DoPlayerStopMove());
            btnMoveRight.triggers.Add(entry9);
        }
    }
}