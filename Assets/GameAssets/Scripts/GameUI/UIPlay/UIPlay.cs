using System.Collections.Generic;
using GameAssets.Scripts.GameBase;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameUI.UIPlay
{
    public partial class UIPlay : BaseUI
    {
        [SerializeField] private List<Image> hearts = new List<Image>();
        [SerializeField] private Sprite heartEmpty;

        [SerializeField] private Animator startAnimator;
        [SerializeField] private Button btnPlay;
        [SerializeField] private Button btnWithdraw;
        [SerializeField] private Button btnPause;
        [SerializeField] private Button btnShoot;

        [SerializeField] private EventTrigger btnJump;
        [SerializeField] private EventTrigger btnMoveLeft;
        [SerializeField] private EventTrigger btnMoveRight;
    }
}