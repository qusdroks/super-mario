using GameAssets.Scripts.GameManager;

namespace GameAssets.Scripts.GameUI.UIGameStart
{
    public partial class UIGameStart
    {
        private void Awake()
        {
            CreatePage();

            for (var x = 0; x < LevelManager.Instance.AllLevels.Count; x++)
            {
                if (_pages[_totalPage].childCount < 10)
                {
                    CreateLevel(x);
                }
                else
                {
                    CreatePage();
                    CreateLevel(x);
                    _pages[_totalPage].gameObject.SetActive(false);
                }
            }

            btnPrevPage.gameObject.SetActive(false);

            btnPrevPage.onClick.AddListener(() =>
            {
                btnNextPage.gameObject.SetActive(true);
                _pages[_indexCurrentPage - 1].gameObject.SetActive(true);
                _pages[_indexCurrentPage].gameObject.SetActive(false);
                _indexCurrentPage--;

                if (_indexCurrentPage == 0)
                {
                    btnPrevPage.gameObject.SetActive(false);
                }
            });

            btnNextPage.onClick.AddListener(() =>
            {
                _indexCurrentPage++;
                btnPrevPage.gameObject.SetActive(true);

                if (_indexCurrentPage == _totalPage)
                {
                    btnNextPage.gameObject.SetActive(false);
                }
                else
                {
                    _pages[_indexCurrentPage].gameObject.SetActive(true);
                    _pages[_indexCurrentPage - 1].gameObject.SetActive(false);
                }
            });

            btnExit.onClick.AddListener(() => gameObject.SetActive(false));
        }
    }
}