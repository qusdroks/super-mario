using System.Collections;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameManager;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameUI.UIWithdraw
{
    public partial class UIWithdraw : BaseUI
    {
        [Header("[GAME OBJECT]")] [SerializeField]
        private GameObject popup;

        [Header("[BUTTON]")] [SerializeField] private Button btnBack;
        [SerializeField] private Button btnMax;
        [SerializeField] private Button btnWithdraw;
        [SerializeField] private Button btnPopupOk;

        [Header("[INPUT FIELD]")] [SerializeField]
        private TMP_InputField inputAddress;

        [SerializeField] private TMP_InputField inputNetwork;
        [SerializeField] private TMP_InputField inputAmount;

        [Header("[TEXT]")] [SerializeField] private TextMeshProUGUI txtAvailable;

        [SerializeField] private TextMeshProUGUI txtFee;
        [SerializeField] private TextMeshProUGUI txtResult;
        [SerializeField] private TextMeshProUGUI txtPopupMessage;

        private float _coin;
        private float _fee;

        private bool _loading;

        private void ShowPopup(string text)
        {
            popup.SetActive(true);
            txtPopupMessage.text = text;
        }

        private void OnValueChanged(string amount)
        {
            if (!float.TryParse(amount, out var coin))
            {
                btnWithdraw.interactable = false;
                return;
            }

            _fee = coin * 0.01f;
            _coin = coin - _fee;
            
            btnWithdraw.interactable = true;
            txtResult.text = $"{coin:N1} Mario";
            txtFee.text = $"Phí mạng lưới: {_fee:N1} Mario";
        }

        private IEnumerator Withdraw()
        {
            _loading = true;
            
            var url = $"https://us-central1-treasure-hunter-unity.cloudfunctions.net/withdraw?" +
                      $"address={inputAddress.text}&id={inputNetwork.text}&amount={_coin}";

            using var www = UnityWebRequest.Get(url);
            yield return www.SendWebRequest();

            if (!www.result.Equals(UnityWebRequest.Result.ConnectionError) && www.isDone)
            {
                _loading = false;
                ShowPopup("Withdraw success");
                UserDataManager.Instance.AddMoney(-_coin);
            }
            else
            {
                _loading = false;
                ShowPopup("Withdraw failed");
            }
        }
    }
}