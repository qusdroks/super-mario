using GameAssets.Scripts.GameBase;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameUI.UIGameComplete
{
    public partial class UIGameComplete : BaseUI
    {
        [SerializeField] private Button btnGameComplete;
        [SerializeField] private Button btnExit;
        [SerializeField] private Button btnNextLevel;
    }
}