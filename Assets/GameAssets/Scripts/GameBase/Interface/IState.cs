﻿using UnityEngine;

namespace GameAssets.Scripts.GameBase.Interface
{
    public interface IState
    {
        BaseCharacter Character { get; set; }
        void StartState(MonoBehaviour target = null);
        void UpdateState();
        void ExitState();
    }
}